import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import Option from './routes/Navigation';
import SolanaSendForm from './components/SendCertificateForm';
import SolanaGetData from './routes/SolanaGetForm';

const rootElement = document.getElementById("root");

ReactDOM.render(
  <BrowserRouter>
    <Routes>
      <Route  path="/" element={<Option />}>
        <Route path="SendData" element={<SolanaSendForm />} />
        <Route path="GetData" element={<SolanaGetData />} />
      </Route>
    </Routes>
  </BrowserRouter>,
  rootElement
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
