import React from 'react';
import moment from 'moment';

interface Values {
    max_age: number
    creation_time: number
    diary_id: number
    number_plate: string
    seal_id: string
    quality_charateristics: number
    cargo: string
}


interface MyState {
    value: string,
    version: number
}


type MyProps = {data: string};
type Mystate = {value: string};


class CertificateTable extends React.Component<MyProps, MyState> {

    
    render() {
        let data = this.props.data;
        let items = {
            max_age: 'NAN',
            creation_time: 'NAN',
            dairy_id: 'NAN',
            number_plate: 'NAN',
            seal_id: 'NAN',
            quality_characteristics: 'NAN',
            cargo: 'NAN',
        }

        if(data.length > 0) {

            items = JSON.parse(data);

            items.creation_time += '000';
        }

        return (
            <div className="row">
                    <div className="col-md-4 ml-1">
                        <table className="table">
                            <thead className="thead-dark">
                                <tr>
                                    <th className="col-md-3" scope="col">Name</th>
                                    <th className="col-md-9" scope="col">Wert</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">Ablaufzeit</th>
                                    <td>{items.max_age}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Zeitstempel</th>
                                    <td>{moment(new Date(parseInt(items.creation_time))).format("DD-MM-YYYY HH:mm")}</td>
                                </tr>
                                
                                <tr>
                                    <th scope="row">MolkereiID</th>
                                    <td>{items.dairy_id}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Kennzeichen</th>
                                    <td>{items.number_plate}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Plombenid</th>
                                    <td>{items.seal_id}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Qualitätsmerkmal</th>
                                    <td>{items.quality_characteristics}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Transportgut</th>
                                    <td>{items.cargo}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        )
    }
}

export default CertificateTable;