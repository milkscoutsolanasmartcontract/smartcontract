import React from 'react';

interface MyState {
    value: string,
    version: number
}

type MyProps = {data: string};
type Mystate = {value: string}; 

class CertificateIdTable extends React.Component<MyProps, MyState>{

    render(){
        let data = this.props.data;
        let items = {
            cert_id: 'NAN'
        }

        if(data.length > 0)
            items = JSON.parse(data);


        return(

            <div className='row'>
                <div className="col-md-12">
                    <table className='table'>
                        <thead className="thead-dark">
                            <tr>
                                <th className="col-md-3" scope="col">Name</th>
                                <th className="col-md-9" scope="col">Wert</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Certficate-ID</th>
                                <td>{items.cert_id}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        )

    }

}

export default CertificateIdTable;