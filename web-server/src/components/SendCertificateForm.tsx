import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Formik, Field, Form, FormikHelpers, validateYupSchema, useFormikContext, useField} from 'formik';
import FormikDateTime from './FormikDateTime';
import 'react-datetime/css/react-datetime.css';
import * as Yup from 'yup';
import Certificate_ID from './CertificateIdTable';


interface Values {
    max_age: number
    creation_time: bigint
    dairy_id: number
    number_plate: string
    seal_id: string
    quality_characteristics: number
    cargo: string
}

type MyState = {data: string, newInfo: number}

const SignupSchema = Yup.object().shape({

    max_age: Yup.string()

        .min(1, 'Too Short!')

        .max(16, 'Too Long!')

        .required('Required'),

    creation_time: Yup.string()

        .min(1, 'Too Short!')

        .max(16, 'Too Long!')

        .required('Required'),

    dairy_id: Yup.string()

        .min(1, 'Too Short!')

        .max(16, 'Too Long!')

        .required('Required'),

    number_plate: Yup.string()

        .min(1, 'Too Short!')

        .max(16, 'Too Long!')

        .required('Required'),

    seal_id: Yup.string()

        .min(1, 'Too Short!')

        .max(16, 'Too Long!')

        .required('Required'),

    quality_characteristics: Yup.string()

        .min(1, 'Too Short!')

        .max(16, 'Too Long!')

        .required('Required'),


    cargo: Yup.string()

        .min(1, 'Too Short!')

        .max(16, 'Too Long!')

        .required('Required'),


});

function toObject(values): string{
    return JSON.stringify(values, (key, value) => 
        typeof value === 'bigint'
            ? value.toString()
            : value
    ) 
}


class SendCertificateForm extends React.Component<{}, MyState> {

    constructor(props){
        super(props);

        this.state =  {
            data: "",
            newInfo: 0
        }
    }
    render(){
        
        let info = <Certificate_ID data = {this.state.data} />;


        return (
            <div className='row m-1'>
                <div className='col-md-4'>
                <Formik
                    initialValues={{
                        max_age: 0,
                        creation_time: BigInt(0),
                        dairy_id: 0,
                        number_plate: "",
                        seal_id: "",
                        quality_characteristics: 0,
                        cargo: ""
                    }}
                    validationSchema={SignupSchema}
                    onSubmit={(
                        values: Values,
                        { setSubmitting }: FormikHelpers<Values>
                    ) => {
                        fetch('http://localhost:5000/sendForm', {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: toObject(values),
                        })
                            .then(res => res.json())
                            .then(data => {
                                console.log(data);
                                this.setState({
                                    data: data,
                                    newInfo: this.state.newInfo + 1
                                })
                            });
                        setSubmitting(false);
                    }}
                >
                    {({ errors, touched }) => (
                        <Form>
                            <div className="col-md-12" >
                                <div className="form-group">
                                    <label htmlFor="max_age">Ablaufzeit:</label>
                                    <Field id="max_age" className="form-control" name="max_age" />
                                    {errors.max_age && touched.max_age ? (

                                        <div>{errors.max_age}</div>

                                    ) : null}
                                </div>
                                <div className="form-group">
                                    <label>Zeitstempel:</label>
                                    <Field id="creation_time" name="creation_time" component={FormikDateTime}/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="dairy_id">MolkereiID</label>
                                    <Field id="dairy_id" className="form-control" name="dairy_id" />
                                    {errors.dairy_id && touched.dairy_id ? (

                                        <div>{errors.dairy_id}</div>

                                    ) : null}
                                </div>
                                <div className="form-group">
                                    <label htmlFor="number_plate">Kennzeichen</label>
                                    <Field id="number_plate" className="form-control" name="number_plate" />
                                    {errors.number_plate && touched.number_plate ? (

                                        <div>{errors.number_plate}</div>

                                    ) : null}
                                </div>
                                <div className="form-group">
                                    <label htmlFor="seal_id">PlombenID</label>
                                    <Field id="seal_id" className="form-control" name="seal_id" />
                                    {errors.seal_id && touched.seal_id ? (

                                        <div>{errors.seal_id}</div>

                                    ) : null}
                                </div>
                                <div className="form-group">
                                    <label htmlFor="quality_characteristics">Qualitätsmerkmal</label>
                                    <Field id="quality_characteristics" className="form-control" name="quality_characteristics" />
                                    {errors.quality_characteristics && touched.quality_characteristics ? (

                                        <div>{errors.quality_characteristics}</div>

                                    ) : null}
                                </div>
                                <div className="form-group">
                                    <label htmlFor="cargo">Transportgut</label>
                                    <Field id="cargo" className="form-control" name="cargo" />
                                    {errors.cargo && touched.cargo ? (

                                        <div>{errors.cargo}</div>

                                    ) : null}
                                </div>

                                <button type="submit" className="btn btn-primary">Absenden</button>
                            </div>
                        </Form>

                    )}
                </Formik>
                </div>
                <div className='col-md-8' >
                    {info}
                </div>
            </div>
        );
    }

}

export default SendCertificateForm;