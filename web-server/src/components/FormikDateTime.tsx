import DateTime from 'react-datetime';
import moment from 'moment';

const DATE_FORMAT = 'DD-MM-YYYY';
const TIME_FORMAT = 'HH:mm';

const FormikDateTime = ({field, form, timeFormat}) => {
    const onFieldChange = value => {
        let dateValue = value;

        if(value instanceof moment){
            dateValue = BigInt(moment(value).unix());
        }

        form.setFieldValue(field.name, dateValue);
    }

    const onFieldBlur = () => {
        form.setFieldTouched(field.name, true);
    }

    return(
        <DateTime
            dateFormat={DATE_FORMAT}
            timeFormat={TIME_FORMAT}
            //id={field.name}
            //name={field.name}
            onChange={onFieldChange}
            //onBlur={onFieldBlur}
        />
    )
}

export default FormikDateTime;