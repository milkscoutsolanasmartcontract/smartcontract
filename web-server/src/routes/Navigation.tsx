import * as React from 'react';
import { Link, Outlet } from 'react-router-dom';



const Navigation: React.FC = () => {

    return(
        <div className=''>
            <div className='row'>
                <nav style={{
                    borderBottom: "solid 1px",
                    paddingBottom: "1rem"
                    }}
                    className="col-12"
                >
                    <Link to="/SendData" className='col-2'>Create Certificate</Link>
                    
                    <Link to="/GetData" className='col-2'>View Certificate</Link>
                </nav>
            </div>
            <Outlet />
        </div>
        
    );
}

export default Navigation;
