import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import CertificateTable from '../components/CertificateTable';
import {Formik, Field, Form, FormikHelpers} from 'formik'

interface Values {
    cert_id: string
}

type MyState = {data: string, newInfo: number};

class SolanaGetForm extends Component<{}, MyState> {

    constructor(props){
        super(props);

        this.state = {
            data: '',
            newInfo: 0
        }
    }

    render() {
        let info = <CertificateTable data = {this.state.data}/>;

        return (
            <div>
                <Formik 
                    initialValues = {{
                        cert_id: ''
                    }}
                    onSubmit={(

                        values: Values,
                        {setSubmitting}: FormikHelpers<Values> ) => {

                            fetch('http://localhost:5000/getCertificate',{
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify(values),
                            })
                                .then(res => res.json())
                                .then(data => {
                                    this.setState({
                                        data: data,
                                        newInfo: this.state.newInfo + 1
                                    });
                                });
                            

                                setSubmitting(false);

                        }}
                >
                    <Form>
                    <div className="col-md-4">
                        <div className="form-row">
                            <div className="col-auto">
                              <label htmlFor="cert_id">Certificate ID</label>
                            </div>
                            <div className="col">
                                    <Field type='string' className="form-control" id="cert_id" name="cert_id" />
                            </div>
                            <div className="col-2">
                                <button type="submit" className="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        </div>
                    </Form>

                </Formik>
                <div>
                    {info}
                </div>
            </div>
        );
       
    }
}

export default SolanaGetForm;