use borsh::{BorshDeserialize, BorshSerialize};
use solana_program::{
    account_info::{next_account_info, AccountInfo},
    entrypoint,
    entrypoint::ProgramResult,
    msg,
    program_error::ProgramError,
    pubkey::Pubkey,
};
use sha2::{Sha256, Digest};
use num_bigint::BigUint;
use solana_program::sysvar::Sysvar;
use solana_program::sysvar::clock::Clock;
use std::convert::TryInto;

#[derive(BorshSerialize, BorshDeserialize, Debug)]
pub struct TransportCertificate {
    pub magic: u32,
    pub version: u8,
    pub checksum: [u8; 32],
    pub signature: [u8; 64],
    pub max_age: u32,
    pub creation_time: u64,
    pub dairy_id: u32,
    pub number_plate: [u8; 16],
    pub seal_id: [u8; 16],
    pub quality_characteristics: u8,
    pub cargo: [u8; 16],
}

const PUB_KEY_AS_BASE58_STRING: &str =  "C4v6qxpRVaVzgCtFFLPwqbAtroUJK1VPtbPyJfKT1Hh8";

entrypoint!(process_instruction);

pub fn process_instruction(
    program_id: &Pubkey,
    accounts: &[AccountInfo], 
    _instruction_data: &[u8],
) -> ProgramResult {
    let accounts_iter = &mut accounts.iter();
    //receiver #1 (lkw)
    let account = next_account_info(accounts_iter)?;
    if account.owner != program_id {
        msg!("Account does not have the correct program id");
        return Err(ProgramError::IncorrectProgramId);
    }
    //payer signer check
    let payer = next_account_info(accounts_iter)?;
    if !payer.is_signer {
        msg!("Payer account isn't signer");
        return Err(ProgramError::InvalidArgument);
    }
    let transport_certificate = TransportCertificate::try_from_slice(_instruction_data);

    match transport_certificate {
        Ok(certificate) => {return check_certificate(&certificate, payer)}
        Err(error) => {
            msg!("Could not deserialize to TransportCertificate: {}", error);
            return Err(ProgramError::InvalidInstructionData);
        }
    }
}

fn check_certificate(transport_certificate: &TransportCertificate, account_info: &AccountInfo) -> ProgramResult{
    let sender_adress_result = check_sender_adress(&account_info);
    if let Err(err) = sender_adress_result {
        return Err(err)
    }
    
    let creation_result = check_creation_time(transport_certificate.creation_time);
    if let Err(err) = creation_result {
        return Err(err)
    }
    
    let signature_result = check_checksum(transport_certificate);
    if let Err(err) = signature_result {
        return Err(err)
    }
    Ok(())
}

fn check_checksum(transport_certificate: &TransportCertificate) -> ProgramResult{
    let mut checksum = Sha256::new();
    checksum.update(transport_certificate.max_age.to_le_bytes());
    checksum.update(transport_certificate.creation_time.to_le_bytes());
    checksum.update(transport_certificate.dairy_id.to_le_bytes());
    checksum.update(transport_certificate.number_plate);
    checksum.update(transport_certificate.seal_id);
    checksum.update(transport_certificate.quality_characteristics.to_le_bytes());
    checksum.update(transport_certificate.cargo);
    
    let finalized_checksum = checksum.finalize();
    msg!("finalized_checksum: \n{:?}", &finalized_checksum);
    let calculated_checksum_as_bytes = BigUint::from_bytes_be(&finalized_checksum[..]);
    let received_checksum_as_bytes = BigUint::from_bytes_le(&transport_certificate.checksum[..]);
    
    let is_calculated_checksum_equal_to_received_checksum = calculated_checksum_as_bytes.eq(&received_checksum_as_bytes);
    if !is_calculated_checksum_equal_to_received_checksum {
        return Err(ProgramError::InvalidArgument);
    }
    Ok(())
}

fn check_creation_time(creation_time: u64) -> ProgramResult {
    //https://stackoverflow.com/questions/68732603/how-can-a-solana-rust-smart-contract-get-block-height-or-unix-time
    match Clock::get() {
        Ok(n) => {
            let ut : u64 = n.unix_timestamp.try_into().unwrap();
            msg!("ts: {}", ut);
            if creation_time <= ut {
                    return Ok(());
                } else {
                    return Err(ProgramError::InvalidArgument);
                }
        },
        Err(_) => return Err(ProgramError::InvalidArgument)
    }
}

fn check_sender_adress(account_info: &AccountInfo) -> ProgramResult {
    let white_listed_pubkey = PUB_KEY_AS_BASE58_STRING.parse::<Pubkey>().unwrap();
    let white_listed_pubkey_as_bytes = white_listed_pubkey.to_bytes();
    let pubkey_from_sender_as_bytes = account_info.key.to_bytes();
    msg!("pub from white list: \n{:?}", white_listed_pubkey_as_bytes);
    msg!("pub from sender:     \n{:?}", pubkey_from_sender_as_bytes);
    msg!("pub from white list: \n{:?}", white_listed_pubkey);
    msg!("pub from sender:     \n{:?}", account_info.key);
    if white_listed_pubkey_as_bytes == pubkey_from_sender_as_bytes {
        return Ok(());
    } else {
        return Err(ProgramError::InvalidArgument);
    }
}