import express, { request, response } from 'express';
import cors from 'cors';
import * as SolanaLogic from './buisness/solanaLogic';
import Certificate from './Certificate';

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());
const port = 5000;

app.get('/running', function(req, res, next) {
    res.json({msg: 'Server is running'});
});

app.get('/testConnection', async(req, res) => {
    
    let message = {};

    message = await SolanaLogic.testSolanaConnection();

    res.json(message);
});

app.post('/sendForm',  async(request, response) => {

    let message = {};
    let cert: Certificate = JSON.parse(JSON.stringify(request.body));
    
    try{

        let cert_id = await SolanaLogic.sendSolanaForm(cert);

        message = JSON.stringify({cert_id: cert_id});

    } catch(e:unknown){
        if(e instanceof Error){

            console.log(`Error: ${e.message}` );
            message = {Error: e.message};
       }
    }

    response.json(message);
});

app.post('/getCertificate', async(request, response) => {

    let message = {};
    let cert_id: string = JSON.parse(JSON.stringify(request.body)).cert_id;

    try {

        let data = await SolanaLogic.getSolanaCertificate(cert_id);
        message = data;

    }catch(e: unknown){
        if(e instanceof Error){
            console.log(`Error: ${e.message}`)
            message = {Error: e.message};
        }
    }

    response.json(message);
});

app.listen(port, () => {
    //eslint:disable-next-line:no-console
    console.log(`Example app listening at http://localhost:${port}`);
});