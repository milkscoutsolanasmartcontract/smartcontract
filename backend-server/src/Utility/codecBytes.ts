/** @fileOverview Bit array codec implementations.
 *
 * @author Emily Stark
 * @author Mike Hamburg
 * @author Dan Boneh
 */

/**
 * Arrays of bytes
 * @namespace
 */

import sjcl from 'sjcl';

/** Convert from a bitArray to an array of bytes. */
function fromBits(arr: sjcl.BitArray) {
    var out = [], bl = sjcl.bitArray.bitLength(arr), i, tmp: number = 0;
    for (i=0; i<bl/8; i++) {
    if ((i&3) === 0) {
        tmp = arr[i/4];
    }
    out.push(tmp >>> 24);
    tmp <<= 8;
    }
    return out;
    
}
    
/** Convert from an array of bytes to a bitArray. */
export function toBits(bytes: number[]) {
    var out = [], i, tmp=0;
    for (i=0; i<bytes.length; i++) {
    tmp = tmp << 8 | bytes[i];
    if ((i&3) === 3) {
        out.push(tmp);
        tmp = 0;
    }
    }
    if (i&3) {
    out.push(sjcl.bitArray.partial(8*(i&3), tmp));
    }
    return out;
}

//https://newbedev.com/how-to-convert-hex-string-into-a-bytes-array-and-a-bytes-array-in-the-hex-string
// Convert a hex string to a byte array
export function hexToBytes(hex : string) {
    for (var bytes = [], c = 0; c < hex.length; c += 2)
    bytes.push(parseInt(hex.substr(c, 2), 16));
    return bytes;
}

//https://newbedev.com/how-to-convert-hex-string-into-a-bytes-array-and-a-bytes-array-in-the-hex-string
// Convert a byte array to a hex string
export function bytesToHex(bytes : number[]) {
    for (var hex = [], i = 0; i < bytes.length; i++) {
        var current = bytes[i] < 0 ? bytes[i] + 256 : bytes[i];
        hex.push((current >>> 4).toString(16));
        hex.push((current & 0xF).toString(16));
    }
    return hex.join("");
}