export default interface Certificate {
    magic_number: number,
    max_age: string, 
    creation_time: string, 
    dairy_id: string, 
    number_plate: string, 
    seal_id: string, 
    quality_characteristics: string, 
    cargo: string
}