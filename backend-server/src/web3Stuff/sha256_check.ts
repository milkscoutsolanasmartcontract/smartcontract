// Welcome to the TypeScript Playground, this is a website
// which gives you a chance to write, share and learn TypeScript.

// You could think of it in three ways:
//
//  - A location to learn TypeScript where nothing can break
//  - A place to experiment with TypeScript syntax, and share the URLs with others
//  - A sandbox to experiment with different compiler features of TypeScript

/*let max_age : number = 5;
console.log(change8_32to32_8([max_age], 4, true));
console.log(bytesToBits(change8_32to32_8([max_age], 4, true)));
console.log(bytesToASCII(change8_32to32_8([max_age], 4, true)));
console.log(bytesToASCII(change8_32to32_8([max_age], 4, true)));*/
//console.log(bytesToBits(change8_32to32_8([test], 1, true)));
let creationtime = BigInt(6);
console.log(change8_32to32_8([creationtime], 8, true));
console.log(bytesToBits(change8_32to32_8([creationtime], 8, true)));
console.log(bytesToASCII(change8_32to32_8([creationtime], 8, true)));


function checkValid(value : any) {
  var max7bit = 127;
  var max8bit = 255;
  var max32bit = 2147;
  var max64bit = BigInt(125);
  var checks : any[][] = [
    [ [ 124 ], 0, 255 ],
    [ [ 124 ], 0, max32bit ],
    [ [ BigInt(126) ], BigInt(0), BigInt(255) ],
    [ [ 0, 1, 2, 3, 255 ], 0, max7bit ],
  ];
  for (var i = 0; i < checks.length; ++i) {
    var check = checks[i];
    var entries = check[0];
    for (var j = 0; j < entries.length; ++j) {
      switch (typeof entries[j]) {
        case 'number':;
        case 'bigint':
          if (entries[j] < check[1] || entries[j] > check[2]) return false;
          break;
        default: return false;
      }
    }
  }
  return true;
}

// To learn more about the language, click above in "Examples" or "What's New".
// Otherwise, get started by removing these comments and the world is your playground.

function convertToByteArrayBigInt(value: bigint[], bytes: bigint, reverse: boolean){
            //console.log(Buffer.from(this.checksum));

            let result: Uint8Array = new Uint8Array(value.length * Number(bytes));

            let totalShifts = bytes - BigInt(1);
      
            for(let i = BigInt(0); i < value.length; i++){
              
              for(let y = BigInt(0); y < bytes; y++){
                let shifts = (totalShifts - y) * BigInt(8);
                result[Number(i*bytes+y)] = Number((value[Number(i)] & (BigInt(0xFF) << shifts) ) >> shifts);
                
              }
            }
      
            //console.log(result);
      
            var array = Array.from(result);
      
            if(reverse){
              array = array.reverse();
            }
      
            return array;
    }

function change8_32to32_8(value: any, bytes: number, reverse: boolean){

      //console.log(Buffer.from(this.checksum));

      let result: Uint8Array = new Uint8Array(value.length * bytes);

      let shifts = bytes-1;

      for(let i = 0; i < value.length; i++){
        for(let y = 0; y < bytes; y++){
          result[i*bytes+y] = (value[i] & (0xFF << ((shifts - y) * 8)) ) >> ((shifts - y) * 8);
        }
        
       /*
        result[i*4+0] = (this.checksum[i] & 0xFF000000) >> 24;
        result[i*4+1] = (this.checksum[i] & 0x00FF0000) >> 16;
        result[i*4+2] = (this.checksum[i] & 0x0000FF00) >> 8;
        result[i*4+3] = (this.checksum[i] & 0x000000FF) >> 0;
      */
      }

      //console.log(result);
      var array = Array.from(result);
      //console.log(array);
      if(reverse){
        array = array.reverse();
      }
      //console.log(array);

      return array;
    }

/** Convert from an array of bytes to a bitArray. */
function bytesToBits(bytes : any) {
    var out = [];
    for (var i = 0; i < bytes.length; ++i) {
        for (var j = 0; j < 8; ++j) {
            out.push((bytes[i] & (1 << (7 - j))) > 0);
        }
    }
    return out;
}
function bytesToASCII(bytes: any): string {
  //return String.fromCharCode(...bytes);

  let result = '';

  for (var i = 0; i < bytes.length; ++i) {
    result += String.fromCharCode(bytes[i]);
  }

  return result;
}