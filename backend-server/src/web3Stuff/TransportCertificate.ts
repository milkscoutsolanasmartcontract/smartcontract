import {
    Keypair,
    Connection,
    PublicKey,
    LAMPORTS_PER_SOL,
    SystemProgram,
    TransactionInstruction,
    Transaction,
    sendAndConfirmTransaction,
} from '@solana/web3.js';
import fs from 'mz/fs';
import path from 'path';
import * as borsh from 'borsh';
import {getPayer, getRpcUrl, createKeypairFromFile} from './utils';
import sjcl from 'sjcl';
import { toBits, hexToBytes, bytesToHex } from '../Utility/codecBytes';
import {
  KJUR,
} from 'jsrsasign';

const Bs58 = require('base-58');


let connection: Connection;
let payer: Keypair;
let programId: PublicKey;
let transportPubkey: PublicKey;

const PROGRAM_PATH = path.resolve(__dirname, './../../../../solana-contract/dist/program');
const PROGRAM_SO_PATH = path.join(PROGRAM_PATH, 'cleaning_certificate.so');
const PROGRAM_KEYPAIR_PATH = path.join(PROGRAM_PATH, 'cleaning_certificate-keypair.json');

const RSA_PUBLIC_KEY_PATH = path.resolve(__dirname, './../../../key.pem');
const RSA_PRIVATE_KEY_PATH = path.resolve(__dirname, './../../../certificate.pem');

/**
 * The Class for sending a Certificate
 */
export class TransportCertificate {

    magic: number = 0;
    version: number = 0;
    checksum: number[] = [3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    signature: number[] = [4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    max_age: number = 0;
    creation_time: bigint = BigInt(0);
    dairy_id: number = 0;
    number_plate: number[] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    seal_id: number[] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    quality_characteristics: number = 0;
    cargo: number[] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

    constructor(fields: {magic: number, version: number, checksum: number[] | null, signature: number[] | null, max_age: number, dairy_id: number, creation_time: bigint, number_plate: number[], seal_id: number[], quality_characteristics: number, cargo: number[] } | undefined = undefined) {
      if (fields) {
        this.magic = fields.magic;
        this.version = fields.version;
        this.max_age = fields.max_age;
        this.dairy_id = fields.dairy_id;
        this.creation_time = fields.creation_time;
        this.number_plate = fields.number_plate;
        this.seal_id = fields.seal_id;
        this.quality_characteristics = fields.quality_characteristics;
        this.cargo = fields.cargo;
        this.checkCreationTime();
        let signatureInvalid = true;
        //distinguish between createCertifice (2x null) and checkCertificate (2x non-null)
        if (fields.checksum == null && fields.signature == null) {
          //createCertificate
          this.checksum = this.calculateChecksumByteArray();
          let tempSig = this.sign();
          this.signature = hexToBytes(tempSig);
          //let sig3 = bytesToHex(this.signature);
          signatureInvalid = !this.verify();
        }
        if (TransportCertificate.checkValid(this) !== true){
          throw Error ('not valid');
        }
        if (fields.checksum != null && fields.signature != null) {
          this.signature = fields.signature;
          this.checksum = fields.checksum;
          //checkCertificate
          signatureInvalid = !this.verify();
        }
        if (signatureInvalid) {
          throw Error('signature');
        }
      }
    }

    static checkValid(value : TransportCertificate) {
      var max7bit = 127;
      var max8bit = 255;
      var maxU32bit = 4294967295;
      var maxU64bit = BigInt('18446744073709551615');

      var checks : any[][] = [
        [ [ value.magic ], 0, maxU32bit, 1 ],
        [ [ value.version ], 0, max8bit, 1 ],
        [ value.checksum, 0, max8bit, 32 ],
        [ value.signature, 0, max8bit, 64 ],
        [ [ value.max_age ], 0, maxU32bit, 1 ],
        [ [ value.creation_time ], BigInt(0), maxU64bit, 1 ],
        [ [ value.dairy_id ], 0, maxU32bit, 1 ],
        [ value.number_plate, 0, max7bit, 16 ],
        [ value.seal_id, 0, max7bit, 16 ],
        [ [ value.quality_characteristics ], 0, max8bit, 1 ],
        [ value.cargo, 0, max7bit, 16 ],
      ];

      for (var i = 0; i < checks.length; ++i) {
        var check = checks[i];
        var entries = check[0];

        var expectedLength = check[3];
        if(expectedLength !== entries.length) return false;

        var minValueInclusive = check[1];
        var maxValueInclusive = check[2];

        for (var j = 0; j < entries.length; ++j) {
          switch (typeof entries[j]) {
            case 'number':
            case 'bigint':
            case 'object': //BN bigint variant #2?
              if (!(entries[j] >= minValueInclusive && entries[j] <= maxValueInclusive)) {
                return false;
              }
              break;
            default: return false;
          }
        }
      }
      return true;
    }
    

    calculateChecksumByteArray() : number[] {
      let temp = this.calculateChecksum();
      return this.convertToByteArray(temp, 4, true);
    }

    calculateChecksum(): sjcl.BitArray{

      var checksum = new sjcl.hash.sha256();

      checksum.update(toBits(this.convertToByteArray([this.max_age], 4, true)));
      checksum.update(toBits(this.convertToByteArrayBigInt([this.creation_time], BigInt(8), true)));
      checksum.update(toBits(this.convertToByteArray([this.dairy_id], 4, true)));
      checksum.update(toBits(this.number_plate));
      checksum.update(toBits(this.seal_id));
      checksum.update(toBits(this.convertToByteArray([this.quality_characteristics], 1, true)));
      checksum.update(toBits(this.cargo));

      return checksum.finalize();
    }

    convertToByteArrayBigInt(value: bigint[], bytes: bigint, reverse: boolean){

      let result: Uint8Array = new Uint8Array(value.length * Number(bytes));

      let totalShifts = bytes - BigInt(1);

      for(let i = BigInt(0); i < value.length; i++){
        
        for(let y = BigInt(0); y < bytes; y++){
          let shifts = (totalShifts - y) * BigInt(8);
          result[Number(i*bytes+y)] = Number((value[Number(i)] & (BigInt(0xFF) << shifts) ) >> shifts);
          
        }
      }

      var array = Array.from(result);

      if(reverse){
        array = array.reverse();
      }

      return array;
    }

    convertToByteArray(value: number[], bytes: number, reverse: boolean){

      let result: Uint8Array = new Uint8Array(value.length * bytes);

      let shifts = bytes-1;

      for(let i = 0; i < value.length; i++){
        
        for(let y = 0; y < bytes; y++){
          
          result[i*bytes+y] = (value[i] & (0xFF << ((shifts - y) * 8)) ) >> ((shifts - y) * 8);
          
        }
      }

      var array = Array.from(result);

      if(reverse){
        array = array.reverse();
      }

      return array;
    }

    /** Convert from an array of bytes to a bitArray. */
    bytesToBits(bytes : any): sjcl.BitArray {
        var out:sjcl.BitArray = [];
        for (var i = 0; i < bytes.length; ++i) {
            for (var j = 0; j < 8; ++j) {
                out[(i+1)*7-j] = ((bytes[i] & (1 << j)) > 0 ? 1:0);
                //out[(i)*8+j] = ((bytes[i] & (1 << (7 - j))) > 0 ? 1:0);
            }
        }
        return out;
    }

    bytesToASCII(bytes: any): string {
      return String.fromCharCode(...bytes);
    }

    //https://stackoverflow.com/questions/22468958/how-can-i-get-a-certificate-to-verify-my-rsa-digital-signature-using-jsrsasign/53423466#53423466
    sign(){
      let key = fs.readFileSync(RSA_PUBLIC_KEY_PATH, "utf-8");
      var sig = new KJUR.crypto.Signature({"alg": "SHA256withRSA"});
      sig.init(key);
      sig.updateHex(bytesToHex(this.checksum));
      return sig.sign();
    }

    //https://stackoverflow.com/questions/22468958/how-can-i-get-a-certificate-to-verify-my-rsa-digital-signature-using-jsrsasign/53423466#53423466
    verify() {
      if (this.checksum !== null && this.signature !== null) {
        let certificate = fs.readFileSync(RSA_PRIVATE_KEY_PATH, "utf-8");
        var sig = new KJUR.crypto.Signature({"alg": "SHA256withRSA"});
        sig.init(certificate);
        sig.updateHex(bytesToHex(this.checksum));
        return sig.verify(bytesToHex(this.signature));
      }
      return false;
    }
        
    toUInt8Array(bitArray: sjcl.BitArray): Uint8Array {
      return Uint8Array.from(bitArray)
    }
  
    checkCreationTime(){
      var today = new Date();
      var totalMilliseconds = today.getTime();
      if(BigInt(totalMilliseconds) < this.creation_time){
        throw new Error("Creation time of the certificate lies in the future.")
      }
    }
}

/**
 * Borsh Schema for sending the Certificate
 */
const TransportCertificateSchema = new Map([
    [TransportCertificate, {kind: 'struct', fields: [
      ['magic', 'u32'],
      ['version', 'u8'],
      ['checksum', [32]],
      ['signature', [64]],
      ['max_age', 'u32'],
      ['creation_time', 'u64'],
      ['dairy_id', 'u32'],
      ['number_plate', [16]],
      ['seal_id', [16]],
      ['quality_characteristics', 'u8'],
      ['cargo', [16]],
    ]}],
  ]);  
 
const TRANSPORT_CERTIFICATE_SIZE = borsh.serialize(
    TransportCertificateSchema,
    new TransportCertificate(),
).length;

/**
 * 
 * @returns gives the URL and version of the connection example:
 */
export async function establishConnection(): Promise<string> {

    const rpcUrl = await getRpcUrl();
    console.log('rpcUrl: ', rpcUrl);
    connection = new Connection(rpcUrl, 'confirmed');
    const version = await connection.getVersion();

    let connectionData = {rpcUrl: rpcUrl, version: version};
    console.log('Connection to cluster established:', rpcUrl, version);
    
    return JSON.stringify(connectionData);

}

/**
 * Test method to create a Payer if non exist
 */
export async function establishPayer(): Promise<void> {
    let fees = 0;
    if (!payer) {
      const {feeCalculator} = await connection.getRecentBlockhash();
      // Calculate the cost to fund the greeter account
      fees += await connection.getMinimumBalanceForRentExemption(TRANSPORT_CERTIFICATE_SIZE);
      // Calculate the cost of sending transactions
      fees += feeCalculator.lamportsPerSignature * 100; // wag
      payer = await getPayer();
    }
    let lamports = await connection.getBalance(payer.publicKey);
    if (lamports < fees) {
      // If current balance is not enough to pay for fees, request an airdrop
      const sig = await connection.requestAirdrop(
        payer.publicKey,
        fees - lamports,
      );
      await connection.confirmTransaction(sig);
      lamports = await connection.getBalance(payer.publicKey);
    }
    console.log(
      'Using account',
      payer.publicKey.toBase58(),
      'containing',
      lamports / LAMPORTS_PER_SOL,
      'SOL to pay for fees',
    );
}
 
/**
 * Function to check:
 * - the Program exists as File *.so
 * - the Keypair for the program exists *-keypair.json
 * - if the Program has been deployed to the Solana Blockchain
 * - Creates a TransportPubKey
 * - Calculates the minimum Balance for the sending of a contract
 */
export async function checkProgram(): Promise<void> {
    // Read program id from keypair file
    try {
      const programKeypair = await createKeypairFromFile(PROGRAM_KEYPAIR_PATH);
      programId = programKeypair.publicKey;
    } catch (err) {
      const errMsg = (err as Error).message;
      throw new Error(
        `Failed to read program keypair at '${PROGRAM_KEYPAIR_PATH}' due to error: ${errMsg}. Program may need to be deployed with \`solana program deploy dist/program/cleaning_certificate.so\``,
      );
    }
    // Check if the program has been deployed
    const programInfo = await connection.getAccountInfo(programId);
    if (programInfo === null) {
      if (fs.existsSync(PROGRAM_SO_PATH)) {
        throw new Error(
          'Program needs to be deployed with `solana program deploy dist/program/cleaning_certificate.so`',
        );
      } else {
        throw new Error('Program needs to be built and deployed');
      }
    } else if (!programInfo.executable) {
      throw new Error(`Program is not executable`);
    }

    console.log(`Using program ${programId.toBase58()}`);

    // Derive the address (public key) of a transport account from the program so that it's easy to find later.
    const TRANSPORT_SEED = 'milk2';
    transportPubkey = await PublicKey.createWithSeed(
      payer.publicKey,
      TRANSPORT_SEED,
      programId,
    );

    console.log('transportPubkey');
    console.log(transportPubkey.toBase58());


    const TRANSPORT_SEED2 = 'milk2';
    var transportPubkey2 = await PublicKey.createWithSeed(
      payer.publicKey,
      TRANSPORT_SEED2,
      programId,
    );

      console.log('transportPubkey2');
      console.log(transportPubkey2.toBase58());

    // Check if the transport account has already been created
    const transportAccount = await connection.getAccountInfo(transportPubkey);
    if (transportAccount === null) {
      console.log(
        'Creating account',
        transportPubkey.toBase58(),
        'to send transport data to',
      );

      const lamports = await connection.getMinimumBalanceForRentExemption(
        TRANSPORT_CERTIFICATE_SIZE,
      );

      const transaction = new Transaction().add(
        SystemProgram.createAccountWithSeed({
          fromPubkey: payer.publicKey,
          basePubkey: payer.publicKey,
          seed: TRANSPORT_SEED,
          newAccountPubkey: transportPubkey,
          lamports,
          space: 0,
          programId,
        }),
      );

      await sendAndConfirmTransaction(connection, transaction, [payer]);
    }
}

/**
 * 
 * @param certificate the Certifacte that was created with its values
 * @returns returns the certificateID
 */
export async function sendCertificate(certificate: TransportCertificate): Promise<string> {

    let borshData: Buffer = Buffer.from(borsh.serialize(TransportCertificateSchema, certificate));

    let instruction = new TransactionInstruction({
        keys: [
          {pubkey: transportPubkey, isSigner: false, isWritable: true}, //receiver #1 (lkw)
          //todo: receiver #2 (dairy)
          {pubkey: payer.publicKey, isSigner: false, isWritable: false}, //payer / signer
        ],
        programId,
        data: borshData,
    });
    
    console.log(borshData);
    console.log('instruction: ' + JSON.stringify(instruction));

    let transactionConfirmation = await sendAndConfirmTransaction(
        connection,
        new Transaction().add(instruction),
        [payer], //change to receiver
    );

    console.log('transactionConfirmation: ', transactionConfirmation);
    
    return transactionConfirmation;
}

/**
 * 
 * @param cert_id the ID of the Certificate to check
 * @returns returns a Object of TransportCertificateFrom that was received from the Certificate
 */
export async function getCertificate(cert_id: string): Promise<TransportCertificate> {

  const transactionInfo = await connection.getTransaction(cert_id);

  //Base 58: from lib.rs _instruction_data: &[u8]
  
  let instructionData = "" + transactionInfo?.transaction.message.instructions[0].data;
  
  var transportData: TransportCertificate = borsh.deserialize(
    TransportCertificateSchema,
    TransportCertificate,
    Buffer.from(Bs58.decode(instructionData)),
  );

  return transportData;
}

/**
 * Test method for checking Account data
 */
export async function getAccData(): Promise<void> {
  const accountInfo = await connection.getAccountInfo(new PublicKey('CmMixoxrWxMUKvRDo31eay4sChF5tmAY8uzhA646m4AC'));
  //binary [u8]
  let accountData = accountInfo?.data;
  console.log(accountData);
  //console.log(JSON.stringify(accountInfo));
  //console.log(accountInfo);
}
  