import { establishConnection, sendCertificate, checkProgram, establishPayer, TransportCertificate, getCertificate, getAccData} from '../web3Stuff/TransportCertificate';
import Certificate from '../Certificate';

 export async function sendSolanaForm(cert: Certificate): Promise<string> {

    let data = '';

    let transCert = new TransportCertificate({
        magic: 1,
        version: 1,
        checksum: null,
        signature: null,
        max_age: Number.parseInt(cert.max_age),
        creation_time: BigInt(cert.creation_time),
        dairy_id: Number.parseInt(cert.dairy_id),
        number_plate: stringToASCII(cert.number_plate),
        seal_id:      stringToASCII(cert.seal_id),
        quality_characteristics: Number.parseInt(cert.quality_characteristics), // add list/enum and check with this
        cargo:        stringToASCII(cert.cargo),
    });
    try {
        await establishConnection();
        console.log('establishedConnection');
        await establishPayer();
        console.log('establishedPayer');
        await checkProgram();
        console.log('checkedProgram');
        data = await sendCertificate(transCert);

    } catch(err){
        const errMsg = (err as Error).message;
        throw new Error(
          `Error: ${errMsg}`,
        );
    }

    console.log('SendCertificate data: ' + data);

    return data;
}

export async function testSolanaConnection(): Promise<string>{

    let promise = await establishConnection()

    return promise;
}

export async function getSolanaCertificate(cert_id: string): Promise<string> {

    await establishConnection();
    let data: TransportCertificate = await getCertificate(cert_id);

    var cert: Certificate = {
        magic_number: data.magic,
        max_age: data.max_age.toString(),
        creation_time: data.creation_time.toString(),
        dairy_id: data.dairy_id.toString(),
        number_plate: ASCIIToString(data.number_plate),
        seal_id: ASCIIToString(data.seal_id),
        quality_characteristics: data.quality_characteristics.toString(),
        cargo: ASCIIToString(data.cargo),
    };

    return JSON.stringify(cert);
}

export async function getProgram(): Promise<void> {
    await establishConnection();
    await establishPayer();
    await checkProgram();

}

function stringToASCII(value: string, maxLength: number = 16): number[]{

    if(value.length > maxLength){
        throw new Error(`The string '${value}' is longer than it's maximum length: ${maxLength}`)
    }

    let array = new Array<number>(maxLength);

    for(var i = 0; i < value.length; i++){
        if(value.charCodeAt(i) >= 33 && value.charCodeAt(i) <= 127){
            array[i] = value.charCodeAt(i)
        } else{
            throw new Error(`The char ${value} is not allowed.`);
        }
    }

    if(value.length < maxLength){
        for(var i = value.length; i < maxLength; i++){
            array[i] = 0;
        }
    }

    return array;
}

function ASCIIToString(value: number[], maxLength: number = 16): string{

    if(value.length > maxLength){
        throw new Error(`The number array '${value}' is longer than it's maximum length: ${maxLength}`)
    }

    let data: string = '';

    for(var i = 0; i < value.length; i++){
        if(value[i] >= 33 && value[i] <= 127){
            data += String.fromCharCode(value[i]);
        } else if(value[i] === 0){
            data += '';
        } else {
            throw new Error(`The char ${value} is not allowed.`);
        }
    }


    return data;
}

function qualtiyConversion(value:string): number{

    switch(value.toLowerCase()){
        case 'hallal':
            return 1;
        case 'koscher':
            return 2;   
        default:
            return 0;
    }
}